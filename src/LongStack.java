import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class LongStack {

    public static void main(String[] argum) {
    }

    private LinkedList<Long> list = new LinkedList<>();

    LongStack(LinkedList<Long> list) {
        this.list = list;
    }

    LongStack() {

    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        LinkedList<Long> listCopy = new LinkedList<>(list);
        return new LongStack(listCopy);
    }

    public boolean stEmpty() {
        return list.size() == 0;
    }

    public void push(long a) {
        list.add(a);
    }

    public long pop() {
        if (list.isEmpty()) {
            throw new RuntimeException("Stack is empty!");
        }
        Long lastElement = list.getLast();
        list.removeLast();
        return lastElement;
    }

    public void op(String s) {
        if (list.size() < 2) {
            throw new RuntimeException("Not enough numbers!");
        }
        Long b = pop();
        Long a = pop();
        long result;
        switch (s) {
            case "+":
                result = a + b;
                break;
            case "-":
                result = a - b;
                break;
            case "*":
                result = a * b;
                break;
            case "/":
                result = a / b;
                break;
            default:
                throw new RuntimeException("Not suitable action: " + s);
        }
        push(result);

    }

    public void rot() {
        if (list.size() < 3) {
            throw new RuntimeException("Not enough numbers!");
        }
        long first = pop();
        long second = pop();
        long third = pop();

        push(second);
        push(first);
        push(third);

    }

    public void swap() {
        if (list.size() < 2) {
            throw new RuntimeException("Not enough numbers!");
        }
        long first = pop();
        long second = pop();

        push(first);
        push(second);
    }

    public long tos() {
        if (list.isEmpty()) {
            throw new RuntimeException("Stack is empty!");
        }
        return list.getLast();
    }

    @Override
    public boolean equals(Object o) {
        if (stEmpty() && ((LongStack) o).stEmpty())
            return true;
        if (list.size() != ((LongStack) o).list.size()) {
            return false;
        }
        for (int i = 0; i < list.size(); i++)
            if (!((LongStack) o).list.get(i).equals(list.get(i)))
                return false;
        return true;
    }

    @Override
    public String toString() {
        return String.valueOf(list);
    }

    public static long interpret(String pol) {
        String[] elements = pol.split(" ");
        LongStack result = new LongStack();
        for (String el : elements) {
            try {
                int num = Integer.parseInt(el.trim());
                result.push(num);
            } catch (NumberFormatException e) {
                if (el.trim().equals("")) {
                    continue;
                }

                String s = el.trim();
                if (s.equals("ROT")) {
                    result.rot();
                }
                else if (s.equals("SWAP")) {
                    result.swap();
                } else {
                    result.op(s);
                }
            }
        }
        if (result.list.size() == 1)
            return result.pop();
        throw new RuntimeException("Not suitable expression!");

    }
}